﻿using MeridianSystems.Solutions.Proliance.ServiceActivator.FoundationTypes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using piqaLib.Models;
using piqaLib.Services;
using System.Collections.Generic;
using System.Linq;

namespace piqaTests
{
    [TestClass]
    public class ProlianceTests
    {
        [TestMethod]
        public void ShouldGetProlianceDocumentTypes()
        {
            ProlianceService pService = new ProlianceService();
            Assert.IsTrue(pService.GetDocumentTypes().Count > 0);
        }        

        [TestMethod]
        public void ShouldGetProlianceDocumentFields()
        {
            string testDocumentTypeName = DocumentTypeNames.ExpenseOriginalContractDocument;

            ProlianceService pService = new ProlianceService();
            Assert.IsTrue(pService.GetDocumentFields(testDocumentTypeName).Count > 0);
        }

        [TestMethod]
        public void ShouldCreatePageInfoQueryClass()
        {
            string testDocumentTypeName = DocumentTypeNames.ExpenseOriginalContractDocument;            

            ProlianceService pService = new ProlianceService();
            List<DocumentField> documentFields = pService.GetDocumentFields(testDocumentTypeName);
            string[] docFields = documentFields.Select(docField => docField.Name).ToArray();

            Assert.IsTrue(!string.IsNullOrEmpty(pService.BuildPageInfoClass(testDocumentTypeName, docFields)));
        }
    }
}
