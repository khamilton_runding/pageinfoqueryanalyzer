﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using piqaLib.Models;
using System.Linq;
using piqaLib.Services;
using System.Collections.Generic;

namespace PageInfoQueryAnalyzerLib.Services
{    
    public class PQIAHub : Hub
    {
        private IConfiguration Configuration
        {
            get;
        }

        public PQIAHub(IConfiguration config)
        {
            Configuration = config;
        }

        public void getDocumentTypes()
        {
            ProlianceService pService = new ProlianceService();
            List<DocumentType> documentTypes = pService.GetDocumentTypes();

            Clients.Caller.SendAsync("documentTypesResult", documentTypes, new System.Threading.CancellationToken(false));
        }

        public void getDocumentFields(string documentTypeName)
        {
            ProlianceService pService = new ProlianceService();
            List<DocumentField> documentFields = pService.GetDocumentFields(documentTypeName);

            Clients.Caller.SendAsync("documentFieldsResult", documentFields, new System.Threading.CancellationToken(false));
        }

        public void buildPageInfoClass(string documentTypeName, List<DocumentField> selectedFields)
        {
            ProlianceService pService = new ProlianceService();
            string pageInfoClass = pService.BuildPageInfoClass(documentTypeName, selectedFields.Select(selectedField => selectedField.Name).ToArray());

            Clients.Caller.SendAsync("classGenerationResult", pageInfoClass);

        }
    }
}
