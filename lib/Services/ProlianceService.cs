﻿using MeridianSystems.Solutions.Proliance.ServiceActivator.FoundationTypes;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CSharp;

using piqaLib.Models;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;

namespace piqaLib.Services
{
    public class ProlianceService
    {
        public List<DocumentType> GetDocumentTypes()
        {
            List<DocumentType> docTypeNames = new List<DocumentType>();
            FieldInfo[] fieldInfos = typeof(DocumentTypeNames).GetFields(
                BindingFlags.Public | BindingFlags.Static |
                BindingFlags.FlattenHierarchy
            );
                        
            foreach (FieldInfo fieldInfo in fieldInfos)
            {
                if (fieldInfo.IsLiteral && !fieldInfo.IsInitOnly)
                {
                    docTypeNames.Add(new DocumentType(fieldInfo.GetRawConstantValue().ToString(), fieldInfo.Name));
                }
            }

            return docTypeNames;
        }        

        public List<DocumentField> GetDocumentFields(string documentTypeName)
        {
            List<DocumentField> documentFields = new List<DocumentField>();
            DataSet documentSchema = readDocumentSchema(documentTypeName);            
            
            foreach (DataColumn documentColumn in documentSchema.Tables[0].Columns)
            {
                documentFields.Add(new DocumentField(documentColumn.ColumnName, documentColumn.DataType.FullName.ToString()));
            }

            return documentFields;
        }

        public string BuildPageInfoClass(string documentTypeName, string[] selectedFields)
        {
            string builtCode = createPageInfoCSCode(documentTypeName, selectedFields);

            var parsedText = CSharpSyntaxTree.ParseText(builtCode);
            SyntaxNode classNode = null;

            if (parsedText.TryGetRoot(out classNode))
            {
                return classNode.ToFullString();
            }
            else
            {
                return "C# Parsing Error";
            }            
        }

        #region Private Helper Functions
        private DataSet readDocumentSchema(string documentTypeName)
        {
            Assembly currentAssembly = Assembly.GetExecutingAssembly();
            string resourceName = string.Format("piqaLib.Schemas.{0}.xsd", DocumentTypeNames.GetShortName(documentTypeName));
            string schemaResult = string.Empty;

            DataSet documentDataSet = new DataSet();
            documentDataSet.ReadXmlSchema(currentAssembly.GetManifestResourceStream(resourceName));

            return documentDataSet;
        }        

        private string createPageInfoCSCode(string documentTypeName, string[] selectedFields)
        {
            return createPageInfoCSCode("PIQAQuery", documentTypeName, selectedFields);
        }
        private string createPageInfoCSCode(string className, string documentTypeName, string[] selectedFields)
        {
            StringBuilder sbClassBuilder = new StringBuilder();

            sbClassBuilder.AppendLine("namespace PIQA.Queries {");
            sbClassBuilder.AppendLine("public class " + className + " {");
            sbClassBuilder.AppendLine("public DataSet RunQuery(ProlianceConnection connectionInfo) {");
            sbClassBuilder.AppendLine(string.Format("PageInfo piqaQuery = new PageInfo(\"{0}\");", documentTypeName));
            sbClassBuilder.AppendLine(string.Format("string piqaQueryPrefix = \"{0}\";", DocumentTypeNames.GetShortName(documentTypeName)));

            foreach (string selectedField in selectedFields)
            {
                sbClassBuilder.AppendLine("piqaQuery.AddSelectField(string.format(\"{0}_{1}\", piqaQueryPrefix, \"" + selectedField + "\");");
            }

            sbClassBuilder.AppendLine("}");
            sbClassBuilder.AppendLine("}");
            sbClassBuilder.AppendLine("}");

            return sbClassBuilder.ToString();
        }
        #endregion
    }
}
