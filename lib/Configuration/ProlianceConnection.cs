﻿using System;
using System.Collections.Generic;
using System.Text;

namespace piqaLib.Configuration
{
    public class ProlianceConnection
    {
        public string ConnectionUri
        {
            get;
            set;
        }

        public string Org
        {
            get;
            set;
        }
        
        public string Username
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }
    }
}
