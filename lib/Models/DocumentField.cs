﻿using System;
using System.Collections.Generic;
using System.Text;

namespace piqaLib.Models
{
    public class DocumentField
    {
        public string FieldType
        {
            get;
        }

        public string Name
        {
            get;
        }

        public DocumentField(string name, string fieldType)
        {
            Name = name;
            FieldType = fieldType;
        }
    }
}
