﻿namespace piqaLib.Models
{
    public class DocumentType
    {
        public string DocumentTypeName
        {
            get;
        }

        public string DocumentTypeNameShort
        {
            get;
        }

        public DocumentType(string documentTypeName, string documentTypeNameShort)
        {
            DocumentTypeName = documentTypeName;
            DocumentTypeNameShort = documentTypeNameShort;
        }
    }
}
