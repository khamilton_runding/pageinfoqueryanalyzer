const mix = require('laravel-mix');

mix.setPublicPath('wwwroot');
mix.webpackConfig({

});

/* COMPILE */
mix.js('Resources/clientApp/piqa.js', 'js/piqa.js')
   .sass('Resources/sass/piqa.scss', 'css/piqa.css')

;