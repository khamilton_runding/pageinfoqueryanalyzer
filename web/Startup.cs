﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PageInfoQueryAnalyzerLib.Services;
using piqaLib.Configuration;

namespace PageInfoQueryAnalyzer
{
    public class Startup
    {
        public IConfigurationRoot Configuration
        {
            get;
            set;
        }

        public Startup(IHostingEnvironment env)
        {
            var configBuilder = new ConfigurationBuilder().SetBasePath(env.ContentRootPath)
                                                          .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            Configuration = configBuilder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            /* CONFIGURE CONFIGURATION */
            services.AddOptions();
            services.Configure<PIQAConfiguration>(Configuration.GetSection("PIQA"));
            
            /* CONFIGURE KESTREL */
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSignalR();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();
            app.UseSignalR(sigR => {
                sigR.MapHub<PQIAHub>("/piqahub");                
            });

            app.UseMvc(router =>
            {
                router.MapRoute(
                    name: "default_route",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Index", action = "Index" }
                );                
            });
        }
    }
}
