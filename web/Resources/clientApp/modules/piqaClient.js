﻿'use strict';

function piqaClient() {
    this.connection = null;
    this.events = [];    
}

piqaClient.prototype = {
    connect() {
        const signalR = require('@aspnet/signalr');
        this.connection = new signalR.HubConnectionBuilder().withUrl('/piqahub').build();        
        
        this.bindSocketEvents(this.connection);

        this.connection.start().then(function() {
            this.runEvent('hubConnected');
        }.bind(this));
    },

    bindSocketEvents(connection) {
        connection.on('documentTypesResult', function(args) {
            this.runEvent('documentTypesResult', args);
        }.bind(this));

        connection.on('columnResults', function (args) {
            this.runEvent('columnResults', args);
        }.bind(this));

        connection.on('documentFieldsResult', function (args) {
            this.runEvent('documentFieldsResult', args);
        }.bind(this));

        connection.on('classGenerationResult', function (args) {
            this.runEvent('classGenerationResult', args);
        }.bind(this));
    },

    disconnect: function () {
        this.connection.disconnect();
    },

    sendMessage: function () {  
        const allArguments = Array.prototype.slice.call(arguments);

        this.connection.invoke.apply(this.connection, allArguments);       
    },

    bindEvent: function (eventName, method) {
        this.events.push({
            name: eventName,
            method: method
        });
    },

    runEvent: function (eventName, parameters) {
        for (var i = 0; i < this.events.length; i++) {
            if (this.events[i] !== void 0 &&
                typeof this.events[i].method === 'function' &&
                this.events[i].name === eventName) {
                this.events[i].method(parameters);
            }
        }
    }
};

module.exports = piqaClient;