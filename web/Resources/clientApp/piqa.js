﻿'use strict';

require('bootstrap');

window.Vue = require('vue');
Vue.use(require('vue-events'));
Vue.use(require('vue-highlightjs'));

const _piqaClient = require('./modules/piqaClient');
window.piqaClient = new _piqaClient();

import docTypeSelector from './components/docTypeSelector';
import fieldList from './components/fieldList';
import pageInfo from './components/pageInfo';

new Vue({
    el: 'PageInfoQueryAnalyzer',

    data() {
        return {

        };
    },

    components: {
        'DocTypeSelector': docTypeSelector,
        'FieldList': fieldList,
        'PageInfo': pageInfo
    },

    methods: {

    },

    created() {
        const pClient = window.piqaClient;        

        pClient.bindEvent('hubConnected', function(args) {
            this.$events.$emit('hubConnected', args);
        }.bind(this));

        pClient.bindEvent('documentTypesResult', function (args) {
            this.$events.$emit('documentTypesResult', args);
        }.bind(this));

        pClient.bindEvent('documentFieldsResult', function (args) {
            this.$events.$emit('documentFieldsResult', args);
        }.bind(this));

        pClient.bindEvent('classGenerationResult', function (args) {
            this.$events.$emit('classGenerationResult', args);
        }.bind(this));

        window.piqaClient.connect();
    },

    mounted() {
        
    },

    template: `
        <div class="container h-100">
            <div class="row">
                <div class="col-12">
                    <DocTypeSelector ref="docTypeSelector"></DocTypeSelector>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <FieldList ref="fieldList"></FieldList>
                </div>
                <div class="col-6">
                    <PageInfo></PageInfo>
                </div>
            </div> 
        </div>
    `
});