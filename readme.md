## PageInfo Query Analyzer

PageInfo Query Analyzer will assist a proliance developer in creating robust PageInfo queries.  This is very basic functionality at this time.

###Stack Used
* .Net Core 2.1
* SignalR
* VueJS
* Laravel Mix 

###Compiling WebPack

To compile client files (production)

`npm run prod`

To compile client files (development)

`npm run dev`

To run a watch on modified files during runtime

`npm run watch`

### Running Kestrel

To start the Kestrel and SignalR Services

`dotnet run`

### ToDo
* Needs to show actual page info result set output
* Needs to be able to "join" multiple pageInfo sets together